## Berikut jawaban saya atas pertanyaan di lab.

1. Apakah perbedaan antara JSON dan XML?  
Ada banyak perbedaan antara keduanya. Berikut adalah beberapa perbedaan keduanya.

| XML      | JSON        |
| -------- | ----------- |
| Hanya mendukung tipe data huruf dan angka | Mendukung berbagai tipe data |
| Tidak bisa ada comment | Bisa ditambahkan comment |
| Hanya mendukung encoding UTF-8 | Mendukung berbagai encoding |
| Mengambil value-nya lebih mudah | Mengambil value-nya lebih sulit |

2. Apakah perbedaan antara HTML dan XML?  
Sama seperti nomor 1, ada banyak perbedaannya. Berikut adalah sebagian perbedaan keduanya.

| HTML     | XML         |
| -------- | ----------- |
| Tidak case sensitive | Case sensitive |
| Tag yang ada sudah didefinisikan sebelumnya | Tag didefinisikan sesuai kebutuhan programmer |
| Closing tag tidak wajib ada | Wajib ada closing tag |
| Berfungsi untuk menampilkan data | Berfungsi untuk memindahkan data |

## Referensi

- [JSON VS XML](https://www.guru99.com/json-vs-xml-difference.html)
- [HTML VS XML](https://www.upgrad.com/blog/html-vs-xml/#:~:text=HTML%20and%20XML%20are%20related,language%20that%20defines%20other%20languages.)